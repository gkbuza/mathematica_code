# This GitLab link
https://gitlab.com/gkbuza/mathematica

## Description
Where I collect some Mathematica scripts I want to conveniently share with other people.

## Contact
gkbnotes@gmail.com
